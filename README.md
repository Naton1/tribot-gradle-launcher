TRiBot Gradle Launcher
======================

This is a Gradle project which allows you to run TRiBot easily.

Requirements
------------
- You must have Java installed to run Gradle.
- Java 15 must be the default Java. Otherwise, see *JDK Configuration* to use Java 15.

JDK Configuration
-------------
If you wish to change the JDK used, uncomment the line in *gradle.properties* and replace the existing path with the path of the JDK you want to use.

Running
-------
The terminal (command prompt) should be used.

### Simple
- Windows: $`gradlew.bat run`
- Mac OS and Linux: $`./gradlew run`

### Without the Gradle process (Detached Mode)
- Windows: $`gradlew.bat runDetached`
- Mac OS and Linux: $`./gradlew runDetached`

### With custom heap size
- Windows: $`gradlew.bat run --mem 1024` (1024MB)
- Mac OS and Linux: $`./gradlew run --mem 1024` (1024MB)

### With CLI arguments
- Windows: $`gradlew.bat run --sid XXX --script XXX` _Along with other arguments_
- Mac OS and Linux: $`./gradlew run --sid XXX --script XXX` _Along with other arguments_

### With alternate TRiBot version (latest is default)
- Windows: $`gradlew.bat run -PtribotVersion=12.0.1-rc1`
- Mac OS and Linux: $`./gradlew run -PtribotVersion=12.0.1-rc1`
